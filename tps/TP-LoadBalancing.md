Création d' un équilibreur de charge standard sur AZURE (loadbalancing)

1. Créer un équilibreur de charge standard
 * Créer une ressource -> mise en réseau -> équilibreur de charge  (ou directement chercher dans la barre de recherhe ```équilibreur de charge```)
 * Créer un groupe de ressource
 * Donner un nom 
 * Passer sku en standard
 * Créer une adresse ip publique
 * Zone de disponibilité -> redondant dans une zone 
 * créer

2. Créer un pool d’adresses principal ( liste des ressources à gerer par le loadbalancing)
 * Se rendre dans l’équilibreur de charge précédemment créé
 * Aller dans paramètre -> pools principaux -> ajouter
 * ajouter les ressources à gerer (les VM par exemple) 
 * Donner un nom 
 * ajouter 

3. Créer une sonde d’intégrité (pour tester regulièrement si tout vas bien)
 * Se rendre dans l’équilibreur de charge précédemment créé 
 * Aller dans paramètre -> sonde d’intégrité -> ajouter 
 * Donner un nom 
 * Protocole -> http 
 * Port -> 80 
 * Intervalle -> 15 
 * Seuil de défaillance -> 2 
 * ok 

4. Créer une règle d’équilibreur de charge 
 * Se rendre dans l’équilibreur de charge précédemment créé
 * Aller dans paramètre -> règles d’équilibreur de charge -> ajouter
 * Donner un nom 
 * Protocole -> tcp 
 * Port -> 80 
 * Port principal : 80 
 * Pool principal : ```<nom de votre pool>``` 
 * Sonde d’integrité : ```<nom de votre sonde>``` 
 * ok

 5.  Créer un réseau virtuel ( ou reprendre un existant)
 * Créer une ressource -> réseau -> réseau virtuel 
 * Groupe de ressource -> prendre celui créé précédemment 
 * Donner un nom
 * Onglet adresse ip -> ipv4 : ```10.1.0.0/16``` 
 * Créer un sous-réseau : ```10.1.0.0/24``` 
 * Enregistrer 
 * Vérifier
 * créer


 6. Créer une machine virtuelle (si ce n'est pas dejà fait)
 * Créer une ressource -> windows server 2016 datacenter    
 * Groupe de ressource -> prendre celui créé plus tôt 
 * Donner un nom 
 * Options de disponibilité -> zone de disponibilité -> 1 
 * Onglet mise en réseau 
 * Réseau virtuel -> prendre celui créé précédemment 
 * Sous-réseau -> prendre celui créé 
 * Adresse ip publique -> créer
 * référence sku -> standard -> zone de dispo -> redondant interzone
 * Paramètres d’équilibrage de charge -> équilibreur de charge azure
 * Sélectionner un équilibreur de charge -> prendre celui créé plus tôt 
 * Créer
 * Recommencer pour une deuxième vm

 7.  Installer IIS (un serveur web) sur des machines virtuelles (pour tester notre loadbalancing)
    * Lancer une vm
    * dans Powershell (en mode admin) :
    ```
    Install-WindowsFeature -name Web-Server -IncludeManagementTools

    remove-item  C:\inetpub\wwwroot\iisstart.htm

    Add-Content -Path "C:\inetpub\wwwroot\iisstart.htm" -Value $("Hello World from " + $env:computername)

    ```
8. Recommencer pour la deuxième vm

9. Tester le loadbalancing
 * dans un navigateur, tapez : <@ip du ressource load-balancing>
 * Arretez un serveur et vérifiez si la charge est basculée vers l'autre serveur




