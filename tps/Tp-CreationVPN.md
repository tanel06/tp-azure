# TP creation d'un reseau virtuel sur AZURE

1. Créer un compte de stockage 
    * Créer un nouveau groupe de ressource 
    * Lui donner un nom et laisser les valeurs par défaut

2. Créer un réseau virtuel 
    * Créer un nouveau groupe de ressource 
    * Lui donner un nom
    * Dans adresse IP  pour IPV4 entrez la plage d’adressage suivante : ```10.1.0.0/16``` 
    * Créer un sous-réseau 
    * Lui donner un nom et comme plage d’adressage : ```10.1.0.0/24```

# Test de ping sur deux VM appartenant à un même VPN
1. Créer une machine virtuelle 
    * Utiliser windows server 2016 datacenter
    * Prendre le même groupe de ressource que celui que vous avez créé précédemment
    * Donner un nom à votre vm
    * Donner un nom d’utilisateur (ce nom servira à ce connecter à votre vm plus tard)
    * Donner un mot de passe
    * Port d’entrée public : HTTP(80) et RDP (3389)
    * Dans mise en réseau : sélectionner le réseau créé précédemment et le sous-réseau
    * Dans administration : prendre le compte de stockage créé plus tôt 
    * Créer sa vm 
    * Recommencer ces étapes pour une seconde vm

2. Se connecter à sa VM
    * Portail azure -> machines virtuelles 
    * Chercher le bouton Connecter, choisir RDP 
    * Télécharger le fichier RDP 
    * Ouvrir le fichier RDP
    * Connexion
    * Une fois connecté ouvrir un powershell (en mode admin) dans la vm
    * Entrer ```ping <nom de la vm 2>``` 
    * Le test échoué, taper dans le powershell des deux vm src et dest:
    ```
    New-NetFirewallRule –DisplayName "Allow ICMPv4-In" –Protocol ICMPv4
    ```

